import ink.runner
from ink.runner import libink_runner, ctypes
import extink_event as event

libink_runner.ink_runner_update.restype = ctypes.c_bool
libink_runner.ink_runner_update.argtypes = [ctypes.c_bool]

libink_runner.ink_runner_set_clipboard_text.argtypes = [
        ctypes.c_char_p,
        ctypes.c_bool,
        ]

libink_runner.ink_runner_get_clipboard_text.restype = ctypes.c_char_p
libink_runner.ink_runner_get_clipboard_text.argtypes = [ctypes.c_bool]


def update(force_step):
    return libink_runner.ink_runner_update(force_step)


def exit():
    libink_runner.ink_runner_exit()


def set_clipboard_text(text, alt):
    libink_runner.ink_runner_set_clipboard_text(
            ctypes.c_char_p(text.encode),
            alt)


def get_clipboard_text(alt):
    return libink_runner.ink_runner_get_clipboard_text(alt).decode()


_unhandled_event_func = ctypes.CFUNCTYPE(None, ctypes.POINTER(event.Event))
_post_step_func = ctypes.CFUNCTYPE(None, ctypes.c_float)
_cb_func = ctypes.CFUNCTYPE(None)


class RunnerOptions(ctypes.Structure):
    _fields_ = [
            ("gui_bin_path", ctypes.c_char_p),
            ("assets_bin_path", ctypes.c_char_p),
            ("net_path", ctypes.c_char_p),
            ("post_init", _cb_func),
            ("pre_step", _cb_func),
            ("post_step", _post_step_func),
            ("unhandled_event", _unhandled_event_func),
            ("continuous_step", ctypes.c_bool),
            ("renderer_api", ctypes.c_uint),
            ("window_title", ctypes.c_char_p),
            ("window_width", ctypes.c_uint),
            ("window_height", ctypes.c_uint),
            ("window_borderless", ctypes.c_bool),
            ]

libink_runner.ink_runner.argtypes = [ctypes.POINTER(RunnerOptions)]

def run(gui_bin_path=None,
        assets_bin_path=None,
        net_path=None,
        window_title=None,
        window_width=800,
        window_height=480,
        post_step=None,
        pre_step=None,
        post_init=None,
        unhandled_event=None,
        continuous_step=False):
    opts = RunnerOptions();
    if gui_bin_path:
        opts.gui_bin_path = ctypes.c_char_p(gui_bin_path.encode())
    if assets_bin_path:
        opts.assets_bin_path = ctypes.c_char_p(assets_bin_path.encode())
    if net_path:
        opts.net_path = ctypes.c_char_p(net_path.encode())
    if post_step:
        opts.post_step = _post_step_func(post_step)
    if pre_step:
        opts.pre_step = _cb_func(pre_step)
    if post_init:
        opts.post_init = _cb_func(post_init)
    if unhandled_event:
        def unhandled_event_wrapper(eventp):
            unhandled_event(eventp.contents)
        opts.unhandled_event = _unhandled_event_func(unhandled_event_wrapper)
    if not window_title:
        window_title = "Ink Application"
    opts.continuous_step = 1 if continuous_step else 0
    opts.window_title = ctypes.c_char_p(window_title.encode())
    opts.window_width = window_width
    opts.window_height = window_height
    libink_runner.ink_runner(ctypes.byref(opts))


ink.runner.update = update
ink.runner.exit = exit
ink.runner.set_clipboard_text = set_clipboard_text
ink.runner.get_clipboard_text = get_clipboard_text
ink.runner.run = run
