import random

from cargo import Cargo
import units

class Town:
    def __init__(self, x, y, name, player):
        self.x = x
        self.y = y
        self.player = player
        self.name = name
        self.cargo = Cargo(100, 100, 100, 100, 100, 100)
        self.past_cargo = self.cargo.copy()
        self.cargo_prices = Cargo(1, 3, 5, 10, 15, 20)
        self.gold = 10000
        self.crew = 200
        self.population = 10000

        self.build_points = 0

    def tick(self, map, ships):
        self.build_points += self.population / 100 + self.crew / 75
        self.crew += self.population // 10000

        if self.build_points > 2000:
            self.build_points -= 2000
            ship = units.Ship(self.player, random.choice(list(units.ShipType)), self.x, self.y)

            ship.crew = min(ship.min_crew, self.crew)
            self.crew -= ship.crew
            for c, t in enumerate(ship.cargo):
                ship.cargo[c] = random.randrange(min(ship.availible_cargo_space()//2, self.cargo[c]))
            self.cargo -= ship.cargo
            ship.gold = random.randrange(self.gold // 4)
            self.gold -= ship.gold

            ships.append(ship)

        self.cargo += Cargo(10, 9, 8, 7, 6, 1)

        for i, v in enumerate(self.cargo):
            self.cargo_prices += self.past_cargo[i]/v - 1 + random.random() - 0.5
        self.cargo_prices.floor()
        self.cargo_prices.clamp(1, 1000)

        self.past_cargo = self.cargo.copy()
