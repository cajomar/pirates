import math

def proportion(in1, out1, in2):
    """Returns a direct proportion such that
    in1    in2
    --- == ---
    out1   out2
    """
    # out = k * in
    # k = out / in
    # out2 = (out1 / in1) * in2
    return out1 / in1 * in2

def proportion_exp(in1, out1, in2, exp):
    """Returns a proportion such that
    in1 ** exp    in2 ** exp
    ---------- == ----------
       out1         out2

    If exp < 0, then it becomes an inverse proportion.
    """
    # out = k * in**exp
    # k = out / in**exp
    # out2 = k * in2**exp
    return out1 / in1**exp * in2**exp

def get_proportion_exp(in1, out1, in2, out2):
    """
    in1 ** exp    in2 ** exp
    ---------- == ----------
       out1         out2

    out2 * in1**exp == out1 * in2**exp

    in1**exp    out1
    -------- == ----
    in2**exp    out2

    (in1)           out1
    (---) ** exp == ----
    (in2)           out2

    (in1 / in2) ** exp == out1 / out2
    """
    return math.log(out1/out2, in1/in2)
