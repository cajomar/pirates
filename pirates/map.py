import random
import enum

import noise

from town import Town


TOWN_NAMES = [
    "Anneville",
    "Port Wine",
    "Cape Petrel",
    "Univer City",
    "Run Town",
    "Fore's Peak",
    "Puerto Bowel",
]

players = [
    ("Your", 0xff),
    ("English", 0xff0000ff),
    ("Spanish", 0xffff00ff),
    ("Dutch", 0xaa00ff),
    ("French", 0xaaff),
    ("Pirate", 0xffffffff),
]


class MapTileType(enum.IntFlag):
    BORDER = enum.auto()
    MEADOW = enum.auto()
    WATER = enum.auto()
    HILLS = enum.auto()
    # FOREST = enum.auto()
    # MOUNTAINS = enum.auto()


class Map:
    __slots__ = "size", "tile_types", "towns"

    def __init__(self, size):
        self.size = size
        self.generate_map()

    def is_costal(self, x, y):
        return any(self.get_tile_type(a, b) & MapTileType.WATER
                   for a, b in self.get_neighbors(x, y))

    def generate_map(self):
        self.tile_types = [MapTileType.WATER for t in range(
                self.size * self.size)]
        lowest = 10
        highest = -10
        octaves = 2
        freq = 16.0 * octaves / 6
        for y in range(self.size):
            for x in range(self.size):
                n = noise.pnoise2(x/freq, y/freq, octaves)
                lowest = min(lowest, n)
                highest = max(highest, n)

        print(lowest, highest)

        for y in range(self.size):
            for x in range(self.size):
                n = (noise.pnoise2(x/freq, y/freq, octaves) - lowest) \
                        / (highest - lowest)

                assert 0 <= n <= 1

                n **= 1.8

                if n > 0.75:
                    self.tile_types[y*self.size+x] = MapTileType.HILLS
                elif n > 0.5:
                    self.tile_types[y*self.size+x] = MapTileType.MEADOW

        def place_town(f, x, y):
            # This is a new function so we can break out of the for loops
            a = list(range(f))
            random.shuffle(a)
            for yy in a:
                if y + yy >= self.size:
                    break
                for xx in a:
                    if x + xx >= self.size:
                        break
                    t = self.get_tile_type(x+xx, y+yy)
                    if t ^ MapTileType.WATER and self.is_costal(x+xx, y+yy):
                        town = Town(
                            x + xx,
                            y + yy,
                            TOWN_NAMES[len(self.towns) % len(TOWN_NAMES)],
                            random.choice(list(range(len(players)))))
                        self.towns.append(town)
                        return

        self.towns = []
        f = 10
        for y in range(0, self.size, f):
            for x in range(0, self.size, f):
                place_town(f, x, y)

    def town_on_tile(self, x, y):
        xx = int(x)
        yy = int(y)
        for town in self.towns:
            if xx == town.x and yy == town.y:
                return town
        return None

    def in_bounds(self, x, y):
        return 0 <= x < self.size and 0 <= y < self.size

    def get_tile_type(self, x, y):
        xx, yy = int(x), int(y)
        if self.in_bounds(xx, yy):
            return self.tile_types[yy*self.size+xx]
        return MapTileType.BORDER

    def get_neighbors(self, x, y):
        if y > 0:
            yield x, y - 1
        if x < self.size - 1:
            yield x + 1, y
        if y < self.size - 1:
            yield x, y + 1
        if x > 0:
            yield x - 1, y

    def random_water(self):
        tiles = [(x, y) for y in range(self.size) for x in range(self.size)]
        random.shuffle(tiles)
        for x, y in tiles:
            if self.get_tile_type(x, y):
                yield x, y
