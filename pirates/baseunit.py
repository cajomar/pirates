import vector

from cargo import Cargo


class Unit:
    enterable = ()

    def __init__(self, player, x, y):
        self.player = player
        self.pos = vector.Vec2(x, y)
        self.past_pos = self.pos.copy()
        self.velocity = vector.Vec2(0, 1)
        self.tried_to_enter = None
        self.units_to_avoid = {}
        self.cargo = Cargo()
        self.crew = 0
        self.gold = 0

    def avoid_unit(self, v):
        self.units_to_avoid[v] = 3

    def update_units_to_avoid(self, dt):
        to_delete = []
        for k in self.units_to_avoid:
            self.units_to_avoid[k] -= dt
            if self.units_to_avoid[k] <= 0:
                to_delete.append(k)
        for k in to_delete:
            del self.units_to_avoid[k]

    def availible_cargo_space(self):
        return self.max_cargo - sum(self.cargo)

    def move(self, get_tile_type, dt):
        self.update_units_to_avoid(dt)
        self.tried_to_enter = None
        self.past_pos = self.pos.copy()
        ltt = get_tile_type(*self.pos)

        self.pos.x += self.velocity.x * dt
        ct = tuple(self.pos.int())
        if ltt & self.enterable and not get_tile_type(*ct) & self.enterable:
            if ct not in self.units_to_avoid:
                self.avoid_unit(ct)
                self.tried_to_enter = ct, self.pos.copy()
            self.pos.x = self.past_pos.x

        self.pos.y += self.velocity.y * dt
        ct = tuple(self.pos.int())
        if ltt & self.enterable and not get_tile_type(*ct) & self.enterable:
            if ct not in self.units_to_avoid:
                self.avoid_unit(ct)
                self.tried_to_enter = ct, self.pos.copy()
            self.pos.y = self.past_pos.y

    def get_botateable_cargo(self):
        c = self.cargo.copy()
        o = Cargo()
        botate_priority = ("indigo", "sugar", "tobacco", "coffee", "cannon",
                           "corn")
        for t in botate_priority:
            count = min(sum(c)-self.max_cargo, getattr(c, t))
            if count < 0:
                break
            if count == 0:
                continue
            setattr(c, t, getattr(c, t)-count)
            setattr(o, t, count)

    # For fleet compatibility
    @property
    def subunits(self):
        return [self]
