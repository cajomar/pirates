import random

import ink

import vector
import units
import map
from cargo import Cargo
from nation import Nation
import ai

# IDs:
# Scene/NoneScene = 0
# Main = 1
# InTown = 2
# CargoTransfer = 3
# ShipAhoy = 4
# SeaBattle = 5
# SeaBattleWon = 6
# Empty = 7
# Prompt = 8
# Story = 9
# Command = 10


class Scene:
    __slots__ = "scroll_x", "scroll_y", "source", "listener", "_subscene", \
            "exit_callback", "return_count", "subscene_queue"
    tile_size = 64
    can_be_drawn = True
    id = 0

    def __init__(self, source=None):
        # renderer stuff
        self.scroll_x = 0
        self.scroll_y = 0

        if source:
            self.source = source
            self.source.clear_all()
            self.listener = ink.DataListener()
            self.source.add_listener(self.listener)

        self._subscene = NoneScene()
        self.exit_callback = None
        self.return_count = 1
        self.subscene_queue = []
        print("Creating", self.__class__.__name__)

    def exit(self):
        print("Deleting", self.__class__.__name__)
        if self.exit_callback:
            self.exit_callback()

    @property
    def subscene(self):
        return self._subscene

    @subscene.setter
    def subscene(self, v):
        if self._subscene.id:
            self._subscene.exit()
        self._subscene = v
        # self.source.set_int("subscene", v.id)

    # Called each step. If the return type in an integer then it will remove
    # that many scenes from the scenestack. If it is an instance of Scene it
    # will replace self with it.
    # If it is anything else nothing happens.
    def _step(self, dt):
        for ev in self.listener.get():
            if ev.type == "IMPULSE":
                if ev.field == "exit":
                    return self.return_count
        return 0

    # A wrapper around _step(). Don't override in subscenes.
    def step(self, dt):
        if self.subscene:
            result = self.subscene.step(dt)
            if isinstance(result, Scene):
                self.subscene = result
            elif isinstance(result, int) and result > 0:
                self.subscene = NoneScene()
                return result - 1
            elif isinstance(result, (list, tuple)):
                self.subscene_queue.extend(result)
                self.subscene = NoneScene()
            return 0
        elif self.subscene_queue:
            self.subscene = self.subscene_queue.pop(0)
            return self.step(dt)
        else:
            return self._step(dt)

    # Used by the renderer
    def to_view_pixel(self, x, y):
        px = x*self.tile_size - self.scroll_x
        py = y*self.tile_size - self.scroll_y
        return int(px), int(py)

    def from_view_pixel(self, x, y):
        xx = (x + self.scroll_x) / self.tile_size
        yy = (y + self.scroll_y) / self.tile_size
        return xx, yy

    def get_active_scene(self):
        s = self
        while s.subscene.id:
            s = s.subscene
        return s

    def get_renderable_scene(self):
        s = self
        last = s
        while s.subscene.id:
            if s.can_be_drawn:
                last = s
            s = s.subscene
        return s if s.can_be_drawn else last


class NoneScene(Scene):
    def __init__(self):
        pass

    def __bool__(self):
        return False


class SeaBattleWon(Scene):
    id = 6
    __slots__ = "ship1", "ship2", "ships", "character", "new_trait_type", \
                "towns"
    can_be_drawn = False

    def __init__(self, ships, towns, ship1, ship2, character):
        Scene.__init__(self, ink.source("battle_won"))
        self.ships = ships
        self.ship1 = ship1
        self.ship2 = ship2
        self.towns = towns
        self.character = character
        self.new_trait_type = units.TraitType.NONE
        if not random.randrange(2):
            self.new_trait_type = random.choice(list(units.TraitType))
        self.source.set_int("new_trait_type", self.new_trait_type.value)

    def _step(self, dt):
        return_ = 0
        for ev in self.listener.get():
            if ev.type == "CHANGE":
                if ev.field == "new_trait_slot":
                    self.character.traits[
                            ev.data.get_int(ev.field)] = self.new_trait_type
                elif ev.field == "action":
                    if ev.data.get_int(ev.field) == 0:
                        # Add to fleet
                        self.character.vehicle = merge(self.ship1,
                                                       self.ship2,
                                                       self.ships)
                        return_ = 1
                    elif ev.data.get_int(ev.field) == 1:
                        # Sink it
                        self.ship1.cargo += self.ship2.cargo
                        self.ship1.gold += self.ship2.gold
                        self.ships.remove(self.ship2)
                        return_ = 1
                    elif ev.data.get_int(ev.field) == 2:
                        # Send a prize
                        self.ship2.player = self.ship1.player
                        return_ = (CargoTransfer(self.ship1, self.ship2),
                                   Command(self.ship2, self.towns))
            elif ev.type == "IMPULSE":
                if ev.field == "exit":
                    return_ = 1

        return return_


class BattleShip:
    RELOAD_TIME = 2
    CANNON_RANGE = 1
    CANNON_RANDOMNESS = 10  # In degrees

    def __init__(self, ship, use_ai):
        self.hitpoints = 1000
        self.ship = ship
        self.use_ai = use_ai
        row = [self.RELOAD_TIME] * min(self.ship.cargo.cannon,
                                       self.ship.max_combat_cannon)
        self.cannons = [row.copy()] * 2

    def __getattr__(self, v):
        return getattr(self.ship, v)

    def fire_cannons(self, target):
        diff = target - self.pos
        diff.radians -= self.velocity.radians
        side = diff.y > 0
        for i, c in enumerate(self.cannons[side]):
            if self.cannons[side][i] < 0:
                self.cannons[side][i] = self.RELOAD_TIME

                cb = units.Cannonball()
                cannon_pos = vector.Vec2(
                        self.length / self.max_combat_cannon * (len(
                                self.cannons[0]) - i),
                        self.beam / (2 if side else -2))
                cannon_pos.radians += self.velocity.radians

                cb.pos = self.pos.copy() + cannon_pos

                cb.start_pos = cb.pos.copy()
                cb.velocity = self.velocity.copy()
                cb.from_ship = self
                cb.velocity.degrees += 90 if side else -90
                cb.velocity.degrees += self.CANNON_RANDOMNESS \
                    * random.random() * random.choice((1, -1))
                cb.velocity.magnitude = 10
                yield cb


def point_in_ship(point, ship):
    r = point - ship.pos
    mag = r.magnitude
    rad = r.radians - ship.velocity.radians
    local = vector.Vec2.polar(mag, rad)
    return abs(local.x) < ship.ship.length/2 \
        and abs(local.y) < ship.ship.beam/2


class SeaBattle(Scene):
    id = 5
    __slots__ = "ships", "towns", "character", "cannonballs", "battle_going", \
                "time", "wind", "to_ai", "player_battleship", "player_ship", \
                "enemy_battleship", "enemy_ship", "battleships", "splashes", \
                "map"
    tile_size = 512
    time_factor = 0.3

    def __init__(self, ships, towns, ship1, ship2, character, map):
        Scene.__init__(self, ink.source("battle"))
        self.map = map
        self.cannonballs = []
        self.character = character
        self.source.set_int("new_trait_slot", -1)
        self.battle_going = True

        self.battleships = []

        self.ships = ships
        self.towns = towns

        if isinstance(ship1, units.Fleet):
            ship1.distribute_cargo()
            battleship1 = BattleShip(ship1.flagship, False)
        else:
            battleship1 = BattleShip(ship1, False)

        self.battleships.append(battleship1)
        if isinstance(ship2, units.Fleet):
            ship2.distribute_cargo()
            # Get the ship with the most useable cannons
            s = sorted(ship2.subunits, key=lambda s: s.max_combat_cannon*2
                       - min(s.max_combat_cannon, s.cargo.cannon)
                       + s.crew/20)
            battleship2 = BattleShip(s[0], True)
        else:
            battleship2 = BattleShip(ship2, True)

        self.battleships.append(battleship2)
        self.player_battleship = battleship1
        self.player_ship = ship1
        self.enemy_ship = ship2
        self.enemy_battleship = battleship2

        self.splashes = []

        self.time = 0
        self.to_ai = 0.1
        self.wind = vector.Vec2(1, 0)

        assert ship1 is not ship2
        assert ship1 in self.ships
        assert ship2 in self.ships

    def new_splash(self, cb, is_explosion=False):
        self.cannonballs.remove(cb)
        self.splashes.append([cb.pos, 0, is_explosion])

    def _step(self, dt):
        for i, s in enumerate(self.splashes):
            s[1] += dt
            if s[1] > 0.25:
                self.splashes.pop(i)

        self.time += dt

        for ev in self.listener.get():
            if ev.type == "IMPULSE":
                # TODO Move to ai?
                if ev.field == "fire":
                    self.cannonballs.extend(
                            self.player_battleship.fire_cannons(
                                self.enemy_battleship.pos))
                elif ev.field == "exit":
                    return 1

        if not self.battle_going:
            return 0

        self.to_ai -= dt
        if self.to_ai < 0.0:
            self.to_ai = 0.1
            for ship in self.battleships:
                t = (b2.pos for b2 in self.battleships if b2 is not ship)
                self.battleship_ai(ship, next(t))

        # Move the units
        for ship in self.battleships:
            # TODO Change
            units.Ship.calculate_speed(ship, self.wind)
            ship.velocity.degrees += ship.turn_speed*dt*ship.turning
            ship.pos += ship.velocity * dt * self.time_factor
            for side in range(2):
                for i, c in enumerate(ship.cannons[side]):
                    ship.cannons[side][i] -= dt

        for cb in self.cannonballs:
            cb.pos += cb.velocity * dt * self.time_factor
            # TODO calculate the upwards curve
            if (cb.pos - cb.start_pos).magnitude > BattleShip.CANNON_RANGE:
                self.new_splash(cb)
                continue

            for ship in self.battleships:
                if ship is cb.from_ship:
                    continue
                if point_in_ship(cb.pos, ship):
                    self.new_splash(cb, True)
                    ship.hitpoints -= 100
                    if ship.hitpoints <= 0:
                        self.battle_going = False
                        if ship is self.player_battleship:
                            return self.end_defeat()
                        else:
                            return self.end_victory()

        if self.time > 600:
            return self.end_stalemate()
        elif (self.battleships[0].pos - self.battleships[1].pos).magnitude >\
                20:
            return self.end_timeout()
        return 0

    def end_stalemate(self):
        msg = "The sun sets and ends the battle."
        self.source.set_str("message", msg)
        self.source.set_int("num_message_papers", max(len(msg)//30, 3))
        self.source.trigger("resolve_message")
        self.battle_going = False
        return 0

    def end_timeout(self):
        msg = "A fierce gust of wind drove the ships apart."
        self.source.set_str("message", msg)
        self.source.set_int("num_message_papers", max(len(msg)//30, 3))
        self.source.trigger("resolve_message")
        return 0

    def end_victory(self):
        return SeaBattleWon(self.ships, self.towns, self.player_ship,
                            self.enemy_ship, self.character)

    def end_defeat(self):
        skiff = units.Ship(self.player_ship.player, units.ShipType.SKIFF,
                           *self.player_ship.pos)
        enemy_fleet = merge(self.enemy_ship, self.player_ship, self.ships)
        skiff.crew = 4
        skiff.avoid_unit(enemy_fleet)
        trans = min(10, enemy_fleet.cargo.corn)
        skiff.cargo.corn = trans
        enemy_fleet.cargo.corn -= trans

        self.ships.append(skiff)
        self.character.vehicle = skiff
        msg = "You lose. You and a few remaining followers are placed in a " \
              "skiff and left to drift at sea."
        self.source.set_str("message", msg)
        self.source.set_int("num_message_papers", len(msg)//30)
        self.source.trigger("resolve_message")
        return 0

    def battleship_ai(self, ship, t):
        if ship is self.player_battleship:
            if self.source.get_bool("turn_port"):
                ship.turning = -1
            elif self.source.get_bool("turn_starboard"):
                ship.turning = 1
            else:
                ship.turning = 0
        else:
            diff = t - ship.pos
            # Angle local to ship's direction
            angle = vector.reg_deg(diff.degrees - ship.velocity.degrees)
            if ship.hitpoints > 100:
                if diff.magnitude < ship.CANNON_RANGE:
                    self.cannonballs.extend(ship.fire_cannons(
                            self.player_battleship.pos))
                    # TODO turn so that the ship's broadsides face the target
                    if -90 < angle < 0 or 90 < angle < 180:
                        ship.turning = 1
                    elif -180 < angle < -90 or 0 < angle < 90:
                        ship.turning = -1
                else:
                    # Chase t
                    ship.turning = 1 if angle > 0 else -1


class ShipAhoy(Scene):
    id = 4
    __slots__ = "ships", "towns", "ship1", "ship2", "character", "map"
    can_be_drawn = False

    def __init__(self, ships, towns, ship1, ship2, character, map):
        Scene.__init__(self, ink.source("ship_ahoy"))
        self.ships = ships
        self.towns = towns
        self.ship1 = ship1
        self.ship2 = ship2
        self.character = character
        self.map = map
        self.source.set_int("ship2_player", self.ship2.player)
        self.source.set_int("ship2_type", self.ship2.type)
        self.source.set_int("ship1_type", self.ship1.type)

        for i, s in enumerate(self.ship1.subunits):
            d = self.source.append("fleet")
            d.set_int("id", i)
            d.set_int("type", s.type)

    def exit(self):
        self.ship1.avoid_unit(self.ship2)
        Scene.exit(self)

    def _step(self, dt):
        for ev in self.listener.get():
            if ev.type == "IMPULSE":
                if ev.field == "attack":
                    return SeaBattle(self.ships,
                                     self.towns,
                                     self.ship1,
                                     self.ship2,
                                     self.character,
                                     self.map)
                elif ev.field == "hail_for_news":
                    self.source.set_str("message", "There is no news.")
                    self.source.set_int("num_message_papers", 3)
                elif ev.field == "merge":
                    self.character.vehicle = merge(self.ship1,
                                                   self.ship2,
                                                   self.ships)
                    return 1
                elif ev.field == "order":
                    return Command(self.ship2, self.towns)
                elif ev.field == "exit":
                    return 1
            elif ev.type == "CHANGE":
                if ev.field == "flagship":
                    assert isinstance(self.ship1, units.Fleet)
                    # We're attacking and have chosen a flagship
                    self.ship1.flagship = self.ship1.subunits[
                            ev.data.get_int(ev.field)]
                    return SeaBattle(self.ships,
                                     self.towns,
                                     self.ship1,
                                     self.ship2,
                                     self.character,
                                     self.map)


def merge(a, b, ships):
    assert a is not b
    print(f"Merging {a} and {b}")
    if isinstance(a, units.Fleet):
        b.player = a.player
        ships.remove(b)
        a.add_ship(b)
        return a
    else:
        ships.remove(a)
        ships.remove(b)
        f = units.Fleet(a)
        b.player = a.player
        f.add_ship(b)
        ships.append(f)
        return f


class CargoTransfer(Scene):
    id = 3
    __slots__ = "unit", "other", "prices", "time_left", \
                "transaction_time", "buying_or_selling"
    can_be_drawn = False
    INITAL_SPEED = 0.5

    def __init__(self, unit, other):
        Scene.__init__(self, ink.source("cargo_transfer"))
        self.unit = unit
        self.other = other

        if isinstance(self.other, units.Town):
            self.prices = list(self.other.cargo_prices)
        else:
            self.prices = list(Cargo())

        self.prices.extend([0, 0])

        self.time_left = [0] * len(self.prices)
        self.transaction_time = [self.INITAL_SPEED] * len(self.prices)
        self.buying_or_selling = [0] * len(self.prices)

        self.rebuild()
        self.source.set_int("type1", units.get_unit_type(self.unit))
        self.source.set_int("type2", units.get_unit_type(self.other))
        self.source.set_int("player2", self.other.player)

    def rebuild(self):
        with self.source.rebuild("goods"):
            q = self.source.query("goods")

            def update(i, s, o):
                d = q.match_int("type", i).get()
                d.set_int("count1", int(s))
                d.set_int("count2", int(o))
                d.set_int("price", self.prices[i])

            if hasattr(self.unit, "cargo") and hasattr(self.other, "cargo"):
                for i, (s, o) in enumerate(zip(self.unit.cargo,
                                               self.other.cargo)):
                    update(i, s, o)
            if hasattr(self.unit, "crew") and hasattr(self.other, "crew"):
                update(len(self.unit.cargo), self.unit.crew, self.other.crew)
            if hasattr(self.unit, "gold") and hasattr(self.other, "gold"):
                update(len(self.unit.cargo)+1, self.unit.gold, self.other.gold)

    def transact(self, i, t, f):
        print(f.player, t.player)
        # if is a good and (t has money or same player) and t has some and t
        # has space
        if i < len(t.cargo) and \
                (t.gold >= self.prices[i] or f.player == t.player) and \
                f.cargo[i] > 0 and \
                (not hasattr(t, "max_cargo") or t.availible_cargo_space() > 0):
            t.cargo[i] += 1
            f.cargo[i] -= 1
            if t.player != f.player:
                t.gold -= self.prices[i]
                f.gold += self.prices[i]
            return True
        # elif is pirates and same player and from has crew and t has space
        elif i == len(t.cargo) and t.player == f.player and f.crew > 0 and \
                (not hasattr(t, "max_crew") or t.crew < t.max_crew):
            f.crew -= 1
            t.crew += 1
            return True
        elif i == len(t.cargo) + 1 and t.player == f.player and f.gold > 0:
            f.gold -= 1
            t.gold += 1
            return True
        else:
            return False

    def _step(self, dt):
        for i in range(len(self.transaction_time)):
            self.buying_or_selling[i] = self.source.query("goods").\
                    match_int("type", i).get().get_int("bos")

            if self.buying_or_selling[i]:
                self.time_left[i] -= dt

                if self.time_left[i] <= 0:
                    self.transaction_time[i] *= 0.8
                    if self.buying_or_selling[i] > 0:
                        if not self.transact(i, self.unit, self.other):
                            self.transaction_time[i] = self.INITAL_SPEED
                            self.buying_or_selling[i] = 0
                    else:
                        if not self.transact(i, self.other, self.unit):
                            self.transaction_time[i] = self.INITAL_SPEED
                            self.buying_or_selling[i] = 0
                    self.time_left[i] = self.transaction_time[i]
            else:
                self.time_left[i] = 0

        for ev in self.listener.get():
            if ev.type == "CHANGE":
                if ev.field == "bos":
                    if ev.data.get_int(ev.field) == 0:
                        self.transaction_time[
                                ev.data.get_int("type")] = self.INITAL_SPEED
            elif ev.type == "IMPULSE":
                if ev.field == "exit":
                    return 1

        self.rebuild()


class InTown(Scene):
    id = 2
    __slots__ = "town", "unit"
    tile_size = 64
    can_be_drawn = False

    def __init__(self, unit, town):
        Scene.__init__(self, ink.source("town"))
        self.town = town
        self.unit = unit
        self.source.set_str("name", self.town.name)
        self.source.set_int("player", self.town.player)

    def exit(self):
        self.unit.avoid_unit(self.town)
        Scene.exit(self)

    def _step(self, dt):
        for ev in self.listener.get():
            if ev.type == "IMPULSE":
                if ev.field == "exit":
                    return 1
                elif ev.field == "merchant":
                    self.subscene = CargoTransfer(self.unit, self.town)
        return 0


class Prompt(Scene):
    id = 8
    can_be_drawn = False

    def __init__(self, title, options):
        # Options is a dictionary containing {label: (callback, sidescene)}
        Scene.__init__(self, ink.source("prompt"))
        self.options = options
        self.source.set_str("title", title)
        for o in options:
            d = self.source.append("options")
            d.set_str("Label", o)

    def _step(self, dt):
        for ev in self.listener.get():
            if ev.type == "IMPULSE":
                if ev.field == "selectSingle":
                    # Run the callback
                    o = self.options[ev.data.get_str("Label")]
                    o[0]()
                    return o[1]
        return 0


class Story(Scene):
    id = 9

    def __init__(self, message):
        Scene.__init__(self, ink.source("story"))
        self.source.set_str("message", message)
        self.source.set_int("num_papers", len(message)//30+2)


class Command(Scene):
    id = 10
    __slots__ = "unit", "towns", "num_commands"
    commands = ["Trade", "Anchor", "Hunt Pirates"]

    def __init__(self, unit, towns):
        Scene.__init__(self, ink.source("command"))
        self.unit = unit
        self.towns = towns

        self.num_commands = 0

        for i, n in enumerate(self.commands):
            d = self.source.append("availible_commands")
            d.set_str("text", n)
            d.set_int("value", i)

        for i, c in enumerate(self.unit.commands):
            d = self.source.append("commands")
            d.set_int("id", i)
            d.set_int("type", ai.commands.index(type(c)))
            if isinstance(c, ai.TradeCommand):
                d.set_int("target", self.towns.index(c.target))
            self.num_commands += 1

    def _step(self, dt):
        for ev in self.listener.get():
            if ev.type == "IMPULSE":
                if ev.field == "exit":
                    self.give_unit_orders()
                    return 1
                elif ev.field == "delete":
                    ev.data.remove()
                    self.num_commands -= 1
            elif ev.type == "APPEND":
                print(ev.field)
                self.num_commands += 1
                print(self.num_commands)
        return 0

    def give_unit_orders(self):
        self.unit.commands.clear()
        for i in range(self.num_commands):
            d = self.source.get_child("commands", i)
            t = d.get_int("type")
            if t == 0:
                c = ai.TradeCommand(self.towns[d.get_int("target")], Cargo(1))
            elif t == 1:
                c = ai.AnchorCommand(self.unit.pos.copy())
            elif t == 2:
                c = ai.HuntCommand(self.unit.pos.copy())
            self.unit.commands.append(c)
            print(self.commands[d.get_int("type")],
                  self.towns[d.get_int("target")].name)


class Main(Scene):
    id = 1
    __slots__ = "wind", "time_till_ai", "unit_collide_distance", "ships", \
                "overland_parties", "character", "map", \
                "time_till_simulation_tick", "nations", "treasures"
    tile_size = 256

    def __init__(self):
        Scene.__init__(self, ink.source("game"))

        self.map = map.Map(64)
        rw = self.map.random_water()
        self.overland_parties = []
        self.ships = []
        for p in range(len(map.players)):
            for i in range(10):
                s = units.Ship(p, random.choice(list(units.ShipType)),
                               *next(rw))
                s.crew = random.randint(s.min_crew, s.max_crew)
                for c, t in enumerate(s.cargo):
                    s.cargo[c] = random.randrange(s.availible_cargo_space()//2)
                self.ships.append(s)

        self.character = units.Character()
        self.character.vehicle = next((s for s in self.ships if not s.player))

        self.source.set_float("tile_size", self.tile_size)
        self.wind = vector.Vec2(1, 0)
        self.time_till_simulation_tick = 0
        self.unit_collide_distance = 0.5
        self.nations = []
        for n, c in map.players:
            self.add_nation(n, c)
        self.treasures = []

    def add_nation(self, name, color):
        n = Nation(name, color)
        n.relations = [0] * len(self.nations)
        self.nations.append(n)
        for na in self.nations:
            na.relations.append(0)

    def ship_ai(self, s):
        if s is self.character.vehicle:
            if self.source.get_bool("go_west"):
                s.turning = -1
            elif self.source.get_bool("go_east"):
                s.turning = 1
            else:
                s.turning = 0
            if self.source.get_bool("go_north"):
                s.moving_sail = 1
            elif self.source.get_bool("go_south"):
                s.moving_sail = -1
            else:
                s.moving_sail = 0

    def overland_party_ai(self, o):
        if o is self.character.vehicle:
            if self.source.get_bool("go_west"):
                o.velocity.x = -1
            elif self.source.get_bool("go_east"):
                o.velocity.x = 1
            else:
                o.velocity.x = 0

            if self.source.get_bool("go_north"):
                o.velocity.y = -1
            elif self.source.get_bool("go_south"):
                o.velocity.y = 1
            else:
                o.velocity.y = 0

    def _step(self, dt):
        if self.source.get_bool("paused"):
            return

        for s in self.ships:
            self.ship_ai(s)
            s.ai_step(self.map, self.wind, dt, s is self.character.vehicle)
        for o in self.overland_parties:
            self.overland_party_ai(o)

        # Move the units
        def gttw(x, y):
            return self.map.get_tile_type(x, y)
        for party in self.overland_parties:
            party.move(gttw, dt)

        for ship in self.ships:
            ship.move(self.wind, gttw, dt)

        battles = set()
        dockings = set()
        landings = set()
        embarkations = set()
        entering_towns = set()

        for s in self.ships:
            town = self.map.town_on_tile(*s.tried_to_enter[0]) if \
                    s.tried_to_enter else None
            if s.tried_to_enter and town:
                dockings.add((s, town))
                s.pos = s.past_pos.copy()
                s.velocity.degrees = s.velocity.degrees - 180
            elif s.tried_to_enter and \
                    self.map.get_tile_type(*s.tried_to_enter[0]) \
                    & units.OverlandParty.enterable:
                landings.add((s, s.tried_to_enter[1].tupal()))
            else:
                for s2 in self.ships:
                    if s2 == s or \
                            s2 in s.units_to_avoid or \
                            s in s2.units_to_avoid:
                        continue
                    elif (s.pos-s2.pos).magnitude < self.unit_collide_distance:
                        # Sort them somehow to one will always be first, so
                        # that there won't be duplicates in the set
                        battles.add(tuple(sorted([s, s2], key=id)))
                        break

        for p in self.overland_parties:
            if p.tried_to_enter:
                ship = None
                for s in self.ships:
                    if p.player == s.player and (p.pos - s.pos).magnitude \
                            < self.unit_collide_distance:
                        embarkations.add((p, s))
                        break
            else:
                town = self.map.town_on_tile(*p.pos)
                if town:
                    entering_towns.add((p, town))
                    p.pos = p.past_pos.copy()
                    p.velocity.zero()

        for b in battles:
            if self.character.vehicle in b:
                s = sorted(b, key=lambda s: s != self.character.vehicle)
                self.subscene = ShipAhoy(self.ships,
                                         self.map.towns,
                                         *s,
                                         self.character,
                                         self.map)
            else:
                # TODO resolve...
                pass

        for p in dockings | entering_towns:
            if self.character.vehicle in p:
                assert self.subscene.id == 0
                self.subscene = InTown(*p)
            else:
                p[0].ai_on_docking(p[1])

        for s, pos in landings:
            if self.character.vehicle == s:
                party = units.OverlandParty(s.player, *pos)
                cs = CargoTransfer(s, party)

                def cargo_callback():
                    if not party.crew:
                        s.cargo += party.cargo
                        s.gold += party.gold
                        self.subscene_queue.clear()
                    else:
                        self.overland_parties.append(party)
                        self.character.vehicle = party
                cs.exit_callback = cargo_callback

                cmd = Command(s, self.map.towns)

                self.subscene_queue.append(cs)
                self.subscene_queue.append(cmd)

                self.subscene = Prompt("Land Ho! Do you...", {
                    "land a party": (lambda: 0, 1),
                    "sail away": (lambda: self.subscene_queue.clear(), 1)
                    })
            else:
                # TODO resolve...
                pass

        for p, s in embarkations:
            s.crew = min(s.crew + p.crew, s.max_crew)
            s.cargo += p.cargo
            s.gold += p.gold

            s.velocity = p.velocity.copy()
            s.velocity.magnitude = .5

            self.overland_parties.remove(p)
            if self.character.vehicle == p:
                self.character.vehicle = s

        for ev in self.listener.get():
            if ev.type == "IMPULSE":
                if ev.field == "show_cargo":
                    self.subscene = CargoTransfer(self.character.vehicle,
                                                  units.Overboard())
                elif ev.field == "burry_treasure":
                    t = units.Treasure(self.character.vehicle.player,
                                       self.character.vehicle.pos)

                    def ec():
                        if sum(t.cargo) or t.gold:
                            self.treasures.append(t)
                    self.subscene = CargoTransfer(self.character.vehicle, t)
                    self.subscene.exit_callback = ec
                elif ev.field == "dig":
                    for t in self.treasures:
                        if (t.pos - self.character.vehicle.pos).magnitude \
                                < self.unit_collide_distance:
                            self.character.vehicle.gold += t.gold
                            self.character.vehicle.cargo += t.cargo
                            s = ("Congradulations! You found a chest "
                                 "containing up to {} pieces of gold and {} "
                                 "tons of valuable cargo!"
                                 ).format(t.gold, sum(t.cargo))
                            self.subscene = Story(s)
                            if sum(self.character.vehicle.cargo) \
                                    > self.character.vehicle.max_cargo:
                                t.cargo = self.character.vehicle.\
                                        get_botateable_cargo()
                                self.character.vehicle.cargo -= t.cargo
                            else:
                                self.treasures.remove(t)
                                t.cargo.clear()
                elif ev.field == "command":
                    self.subscene = Command(None, self.map.towns)

        if not self.subscene.id:
            # Update the GUI
            self.source.set_int("vehicle_type", units.get_unit_type(
                self.character.vehicle))
            with self.source.rebuild("towns"):
                q = self.source.query("towns")
                for i, t in enumerate(self.map.towns):
                    d = q.match_int("id", i).get()
                    d.set_str("name", t.name)
                    x, y = self.to_view_pixel(t.x, t.y)
                    d.set_float("draw_x", x)
                    d.set_float("draw_y", y)
                    d.set_int("player", t.player)
            with self.source.rebuild("traits"):
                for i, t in enumerate(self.character.traits):
                    d = self.source.append("traits")
                    d.set_int("slot", i)
                    d.set_int("type", t)

            with self.source.rebuild("players"):
                q = self.source.query("players")
                for i, n in enumerate(self.nations):
                    d = q.match_int("id", i).get()
                    d.set_str("name", n.name)
                    d.set_color("color", n.color)

        self.tick(dt)

    def tick(self, dt):
        self.time_till_simulation_tick -= dt
        if self.time_till_simulation_tick <= 0:
            self.time_till_simulation_tick = 4
        else:
            return
        print("TICKING")
        # Simulation tick. If we kept track of time this would be a day.
        for ship in self.ships:
            # Eat food
            food_to_eat = ship.crew / 100
            if food_to_eat * 7 > ship.cargo.corn:
                food_to_eat *= ship.cargo.corn / (food_to_eat * 7)
            ship.cargo.corn -= food_to_eat

        print(self.character.vehicle.cargo.corn)

        for t in self.map.towns:
            t.tick(self.map, self.ships)
