from os.path import join

import lib2d
import ink

import scene
import units
import map


SHIP_DIRECTIONS = 36
SCROLL_TILES_PER_SECOND = 20

# Main takes care of this
screen_width = 0
screen_height = 0


def datapath(p):
    return join("data", p)


class Animator:
    anims = []
    __slots__ = "span", "time", "inversed"

    def __init__(self, time, inversed):
        self.span = time
        self.time = 0
        self.inversed = inversed
        Animator.anims.append(self)

    @classmethod
    def step(cls, dt):
        for a in cls.anims:
            a.time += dt
            if a.time >= a.span:
                cls.anims.remove(a)

    def get(self):
        if self.inversed:
            return self.time / self.span
        else:
            return (self.span - self.time) / self.span


class Data:
    def __init__(self):
        self.terrain_sprites = {}
        for t in units.MapTileType:
            self.terrain_sprites[t.value] = lib2d.Drawable(
                    datapath(f"{t.name.lower()}.png"))
        self.ship_sprite = lib2d.Drawable(datapath("ship.png"))
        self.explosion_sprite = lib2d.Drawable(datapath("explosion.png"))
        self.splash_sprite = lib2d.Drawable(datapath("splash.png"))
        self.town_sprite = lib2d.Drawable(datapath("town.png"))
        self.army_sprite = lib2d.Drawable(datapath("army.png"))

        self.cannonball_sprite = lib2d.Drawable(
                datapath("cannonball.png"), w=8, h=8)
        self.compass_sprite = lib2d.Drawable(
                datapath("compass.png"), w=32, h=32)


def init():
    global data
    data = Data()


def draw(s):
    if isinstance(s, scene.Main):
        draw_main(s)
    elif isinstance(s, scene.SeaBattle):
        draw_sea_battle(s)


def draw_ship(ship, s, scale):
    d = data.ship_sprite
    d.color = 0xffffffff
    d.rot = ship.velocity.radians
    d.h = ship.beam * s.tile_size * scale
    d.w = ship.length * s.tile_size * scale
    d.x, d.y = s.to_view_pixel(ship.pos.x, ship.pos.y)
    d.x -= d.w/2
    d.y -= d.h/2
    d.draw()
    d = lib2d.Drawable(color=map.players[ship.player][1],
                       w=ship.length*s.tile_size,
                       h=ship.beam*s.tile_size,
                       rot=ship.velocity.radians)

    d.x, d.y = s.to_view_pixel(ship.pos.x, ship.pos.y)
    d.x -= d.w/2
    d.y -= d.h/2
    d.draw()


def draw_backround(s):
    left, top = s.from_view_pixel(0, 0)
    right, bottom = s.from_view_pixel(screen_width, screen_height)

    top = int(top)
    left = int(left)
    right = int(right) + 1
    bottom = int(bottom) + 1

    top = max(top, 0)
    left = max(left, 0)
    right = min(right, s.map.size)
    bottom = min(bottom, s.map.size)

    for y in range(top, bottom):
        for x in range(left, right):
            tile = s.map.get_tile_type(x, y)
            d = data.terrain_sprites[tile]
            d.w = s.tile_size
            d.h = d.w
            d.x, d.y = s.to_view_pixel(x, y)
            d.draw()


def draw_main(s):
    s.scroll_x = s.character.vehicle.pos.x*s.tile_size - screen_width/2
    s.scroll_y = s.character.vehicle.pos.y*s.tile_size - screen_height/2

    draw_backround(s)

    left, top = s.from_view_pixel(0, 0)
    right, bottom = s.from_view_pixel(screen_width, screen_height)

    for ship in s.ships:
        if left <= ship.pos.x <= right and top <= ship.pos.y <= bottom:
            for su in ship.subunits:
                draw_ship(su, s, 2)

    for party in s.overland_parties:
        if left <= party.pos.x <= right and top <= party.pos.y <= bottom:
            d = data.army_sprite
            d.x, d.y = s.to_view_pixel(party.pos.x, party.pos.y)
            d.w = s.tile_size
            d.h = d.w * d.image.h / d.image.w
            d.x -= d.w/2
            d.y -= d.h/2
            d.draw()

    data.compass_sprite.x = 0
    data.compass_sprite.y = 0
    data.compass_sprite.rot = s.wind.radians
    data.compass_sprite.draw()


def draw_sea_battle(s):
    d = (s.enemy_battleship.pos - s.player_battleship.pos)/2 \
            + s.player_battleship.pos
    s.scroll_x = d.x*s.tile_size - screen_width/2
    s.scroll_y = d.y*s.tile_size - screen_height/2

    draw_backround(s)

    for ship in s.battleships:
        draw_ship(ship, s, 1)

    for c in s.cannonballs:
        d = data.cannonball_sprite
        d.x, d.y = s.to_view_pixel(c.pos.x, c.pos.y)
        d.x -= d.w/2
        d.y -= d.h/2
        d.draw()

    for splash in s.splashes:
        d = data.explosion_sprite if splash[2] else data.splash_sprite
        d.x, d.y = s.to_view_pixel(*splash[0])
        d.w = 32 + 64 * (splash[1]/0.25)
        d.h = d.w
        d.x -= d.w/2
        d.y -= d.h/2
        d.color = 0xffffff00 | int(0xff * (0.25-splash[1])/0.25)
        d.draw()

    data.compass_sprite.x = 0
    data.compass_sprite.y = 0
    data.compass_sprite.rot = s.wind.radians
    data.compass_sprite.draw()


def draw_world(s):
    # s.tile_size = min(screen_height/s.map.size, screen_width/s.map.size)
    size = 4
    source = ink.source("world_map")
    source.set_int("size", size * s.map.size)
    ox = source.get_float("x")
    oy = source.get_float("y")

    for y in range(0, s.map.size):
        for x in range(0, s.map.size):
            tile = s.map.get_tile_type(x, y)
            d = data.terrain_sprites[tile]
            d.w = size
            d.h = size
            d.x = x*size + ox
            d.y = y*size + oy
            d.draw()

    if isinstance(s.character.vehicle, units.Ship):
        d = data.ship_sprite
    else:
        d = data.army_sprite
    d.color = 0xffffffff
    d.rot = 0.0
    d.h = size
    d.w = d.image.w / d.image.h * d.h
    d.x = s.character.vehicle.pos.x*size + ox
    d.y = s.character.vehicle.pos.y*size + oy
    d.draw()
