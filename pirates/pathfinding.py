import heapq
import collections

from map import MapTileType

ANGLES = [
        -2.356194490192345, -1.5707963267948966, -0.7853981633974483,
        -3.141592653589793, 0.0, 0.0,
        2.356194490192345, 1.5707963267948966, 0.7853981633974483
        ]

DISTANCES = [
        1.4142135623730951, 1.0, 1.4142135623730951,
        1.0, 0.0, 1.0,
        1.4142135623730951, 1.0, 1.4142135623730951
        ]


class PriorityQueue:
    def __init__(self):
        self.elements = []

    def empty(self):
        return len(self.elements) == 0

    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))

    def get(self):
        return heapq.heappop(self.elements)[1]


def neighbors(pos, map, enterable, enterables):
    for y in range(-1, 2):
        for x in range(-1, 2):
            xy = pos[0]+x, pos[1]+y
            if not (x or y):
                continue
            if not map.in_bounds(*xy):
                continue
            if not (map.get_tile_type(*xy) & enterable or xy in enterables):
                continue
            if x and y and not (
                    map.get_tile_type(xy[0], pos[1]) & enterable and
                    map.get_tile_type(pos[0], xy[1]) & enterable):
                continue
            yield xy, (y+1)*3+x+1


def heuristic(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])


def get_ship_speeds(ship, wind):
    speeds = [0.0]*9
    sv = ship.velocity.copy()
    for yy in range(-1, 2):
        for xx in range(-1, 2):
            i = (yy+1)*3+xx+1
            ship.velocity.set_polar(DISTANCES[i], ANGLES[i])
            ship.calculate_speed(wind)
            speeds[i] = ship.velocity.magnitude
    ship.velocity = sv
    return speeds


def find_path(start, goal, map, ship, wind):
    frontier = PriorityQueue()
    frontier.put(start, 1)
    came_from = {}
    # Stores the ammount of time it would take to the tiles
    cost_so_far = {}
    # came_from[start] = start
    cost_so_far[start] = 0

    speeds = get_ship_speeds(ship, wind)
    times = [0.0]*9
    for i in range(9):
        times[i] = DISTANCES[i] / (speeds[i] + 0.000001)

    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        # assert -1 <= dx <= 1 and -1 <= dy <= 1
        # angle = ANGLES[(current[1] - came_from[current][1] + 1)
        #            * 3 + current[0] - came_from[current][0] + 1]

        for n, i in neighbors(current, map, ship.enterable, {goal}):
            if speeds[i] < 0.000001:
                continue
            # if math.degrees(ANGLES[i] - angle) > DISTANCES[i] / speeds[i] \
            #        * ship.turn_speed:
            #    pass
            new_cost = cost_so_far[current] + times[i]
            if n not in cost_so_far or new_cost < cost_so_far[n]:
                cost_so_far[n] = new_cost
                came_from[n] = current
                priority = new_cost + heuristic(n, goal)
                frontier.put(n, priority)

    path = []
    if goal in came_from:
        p(start, goal, map.size, map.tile_types, ship, came_from)
        current = goal
        while current != start:
            path.append(current)
            current = came_from[current]
    else:
        print("Counldn't find path from", start, "to", goal)
    return path


def find_path_to_nearest(start, goals, map, enterable, wind):
    frontier = collections.Deque()
    frontier.append(start)
    came_from = {start: (0, 0)}

    while frontier:
        current = frontier.popleft()
        if current in goals:
            break

        for n, i in neighbors(current, map, enterable, goals):
            came_from[n] = current
            frontier.append(n)

    path = []
    if current in goals:
        while current != start:
            path.append(current)
            current = came_from[current]
    return path


def p(start, goal, map_size, types, ship, came_from):
    a = ['·' if i == MapTileType.WATER else '#' for i in types]
    arrows = ["↖", "↑", "↗",
              "←", "~", "→",
              "↙", "↓", "↘"]

    for x, y in came_from:
        i = y*map_size+x
        fx, fy = came_from[(x, y)]
        a[i] = arrows[(fy-y+1)*3+fx-x+1]

    a[start[1]*map_size+start[0]] = '@'
    a[goal[1]*map_size+goal[0]] = '$'
    for y in range(len(types)//map_size):
        print(' '.join(a[y*map_size:y*map_size+map_size]))
