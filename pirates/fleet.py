from cargo import Cargo
from ship import Ship
import vector


def flagship_value_wrapper(field):
    def s(self, v):
        setattr(self.flagship, field, v)
    return property(lambda self: getattr(self.flagship, field), s)

class Fleet(Ship):
    def __init__(self, ship):
        self.units_to_avoid = {}
        self.tried_to_enter = None
        self.cargo = Cargo()
        self.crew = 0
        self.gold = 0

        self._subunits = []
        self.flagship = ship
        self.add_ship(ship)

    def move(self, wind, get_tile_type, dt):
        for s in self.subunits:
            s.move(wind, get_tile_type, dt)

    def calculate_speed(self, wind):
        self.flagship.calculate_speed(self, wind)

    @property
    def flagship(self):
        return self._flagship

    @flagship.setter
    def flagship(self, f):
        self._flagship = f

    subunits = property(lambda self: self._subunits)
    player = flagship_value_wrapper("player")
    pos = flagship_value_wrapper("pos")
    past_pos = flagship_value_wrapper("past_pos")
    velocity = flagship_value_wrapper("velocity")
    turning = flagship_value_wrapper("turning")
    moving_sail = flagship_value_wrapper("moving_sail")
    type = flagship_value_wrapper("type")
    turn_speed = flagship_value_wrapper("turn_speed")

    def add_ship(self, ship):
        self.crew += ship.crew
        self.cargo += ship.cargo
        self.gold += ship.gold
        self.subunits.extend(ship.subunits)

    def update_subunits_in_fleet(self):
        for s in self.subunits:
            s.player = self.player
            s.pos = self.pos.copy()
            s.past_pos = self.past_pos.copy()
            s.velocity = self.velocity.copy()
            s.turning = self.turning

    def distribute_cargo(self):
        crew = self.crew
        cargo = self.cargo.copy()
        gold = self.gold

        total_max_combat_cannon = sum(
                s.max_combat_cannon for s in self.subunits)

        for s in self.subunits:
            s.crew = s.min_crew
            crew -= s.min_crew
            s.cargo.clear()
            s.gold = 0

            if self.cargo.cannon > total_max_combat_cannon:
                s.cargo.cannon = s.max_combat_cannon
                cargo.cannon -= s.max_combat_cannon

        assert crew >= 0

        # Distribute cargo
        for i, v in enumerate(cargo):
            for s in self.subunits:
                extra_space = s.max_cargo - sum(s.cargo)
                num = int(extra_space / self.max_cargo * v)
                num = min(num, v, extra_space)
                cargo[i] -= num
                s.cargo[i] += num
            si = 0
            while cargo[i] > 0:
                s = self.subunits[si]
                c = min(cargo[i], 1)
                if s.max_cargo - sum(s.cargo) >= c:
                    s.cargo[i] += c
                    cargo[i] -= c
                si += 1
                si %= len(self.subunits)

        # Distribute crew
        for s in self.subunits:
            extra_space = s.max_crew - s.crew
            num = int(extra_space / self.max_crew * crew)
            num = min(num, crew, extra_space)
            crew -= num
            s.crew += num
        si = 0
        print("Crew:", crew)
        while crew > 0:
            s = self.subunits[si]
            if s.max_crew > s.crew:
                s.crew += 1
                crew -= 1
            si += 1
            si %= len(self.subunits)

        # Distribute gold
        for s in self.subunits:
            num = int(s.max_cargo / self.max_cargo * gold)
            num = min(num, gold)
            gold -= num
            s.gold += num
        si = 0
        print("Gold:", gold)
        while gold > 0:
            s = self.subunits[si]
            s.gold += 1
            gold -= 1
            si += 1
            si %= len(self.subunits)

    min_crew = property(lambda self: sum(s.min_crew for s in self.subunits))
    max_crew = property(lambda self: sum(s.max_crew for s in self.subunits))
    max_cargo = property(lambda self: sum(s.max_cargo for s in self.subunits))
    #turn_speed = property(lambda self: min(
        #s.turn_speed for s in self.subunits))

    def ai_step(self, map, wind, dt, human_controlled):
        for s in self.subunits:
            if s is self.flagship:
                s.ai_step(map, wind, dt, human_controlled)
            else:
                self.ai_tick_ship_in_fleet(s)

    def ai_tick_ship_in_fleet(self, ship):
        max_avoid_distance = 0.5
        mass = vector.Vec2(0, 0)
        velocity = vector.Vec2(0, 0)

        """
        mass = sum([s.pos for s in self.subunits if s is not ship])
        velocity = sum([s.velocity for s in self.subunits if s is not ship])
        """

        avoid = vector.Vec2(0, 0)
        for s in self.subunits:
            if s is ship:
                continue
            dist = s.pos - ship.pos
            if dist.magnitude_squared < max_avoid_distance ** 2:
                avoid -= dist

            velocity += s.velocity
            mass += s.pos

        mass /= len(self.subunits) - 1
        mass -= ship.pos
        velocity /= len(self.subunits) - 1
        velocity.normalize_ip()

        v = mass + velocity + avoid
        v.radians -= ship.velocity.radians

        if v.degrees > ship.turn_speed * self.AI_TIME:
            ship.turning = 1
        elif v.degrees < -ship.turn_speed * self.AI_TIME:
            ship.turning = -1
        else:
            ship.turning = 0

    def ai_on_docking(self, town):
        self.flagship.ai_on_docking(self, town)
