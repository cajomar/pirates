import operator


def transact(t, f, i, c, prices):
    if i < len(t.cargo):
        c = min(c, f.cargo[i])
        if f.player != t.player:
            c = min(c, t.gold // prices[i])
        if hasattr(t, "max_cargo"):
            c = min(c, t.availible_cargo_space())
        t.cargo[i] += c
        f.cargo[i] -= c
        if t.player != f.player:
            t.gold -= int(prices[i] * c)
            f.gold += int(prices[i] * c)
        return c
    elif i == len(t.cargo) and t.player == f.player:
        c = min(c, f.crew)
        if hasattr(t, "max_crew"):
            c = min(c, t.max_crew - t.crew)
        f.crew -= c
        t.crew += c
        return c
    elif i == len(t.cargo) + 2 and t.player == f.player:
        c = min(c, f.gold)
        f.gold -= c
        f.gold += c
        return c
    else:
        return 0


def _cargo_operator(op):
    def f(self, c):
        if isinstance(c, type(self)):
            return self.__class__(op(v, c[i]) for i, v in enumerate(self))
        elif isinstance(c, (float, int)):
            return self.__class__(op(v, c) for i, v in enumerate(self))
        else:
            raise TypeError("Impossible opperation for type {} and type {}".
                            format(type(self), type(c)))
    return f


def _cargo_operator_ip(op):
    def f(self, c):
        if type(c) == type(self):
            for i, c in enumerate(c):
                self[i] = op(self[i], c)
            return self
        elif isinstance(c, (float, int)):
            for i, s in enumerate(self):
                self[i] = op(self[i], c)
            return self
        else:
            raise TypeError("Impossible opperation for type {} and type {}".
                            format(type(self), type(c)))
    return f


class Cargo:
    __slots__ = "corn", "indigo", "sugar", "tobacco", "coffee", "cannon"
    __add__ = _cargo_operator(operator.add)
    __sub__ = _cargo_operator(operator.sub)
    __mul__ = _cargo_operator(operator.mul)
    __truediv__ = _cargo_operator(operator.truediv)
    __floordiv__ = _cargo_operator(operator.floordiv)
    __iadd__ = _cargo_operator_ip(operator.add)
    __isub__ = _cargo_operator_ip(operator.sub)
    __imul__ = _cargo_operator_ip(operator.mul)
    __itruediv__ = _cargo_operator_ip(operator.truediv)
    __ifloordiv__ = _cargo_operator_ip(operator.floordiv)

    def __init__(self, *args):
        if len(args) == 0:
            for c in self.__slots__:
                setattr(self, c, 0)
        elif len(args) == 1:
            if isinstance(args[0], (int, float)):
                for c in self.__slots__:
                    setattr(self, c, args[0])
            else:
                # Assume it's an iterable
                for c, v in zip(self.__slots__, args[0]):
                    setattr(self, c, v)
        elif len(args) == len(self):
            for c, v in zip(self.__slots__, args):
                setattr(self, c, v)
        else:
            print("Expected 0, 1 or {} arguments for initalizing class {}".
                  format(len(self.__slots__), self.__class__.__name__))

    def copy(self):
        return self.__class__(*self)

    def clear(self):
        for c in self.__slots__:
            setattr(self, c, 0)

    def floor(self):
        for c in self.__slots__:
            setattr(self, c, int(getattr(self, c)))

    def round(self):
        for c in self.__slots__:
            setattr(self, c, int(round(getattr(self, c))))

    def clamp(self, mi, ma):
        for c in self.__slots__:
            setattr(self, c, max(mi, min(getattr(self, c), ma)))

    def __iter__(self):
        for c in self.__slots__:
            yield getattr(self, c)

    def __getitem__(self, index):
        return getattr(self, self.__slots__[index])

    def __setitem__(self, index, value):
        setattr(self, self.__slots__[index], value)

    def __len__(self):
        return len(self.__slots__)

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, list(self))
