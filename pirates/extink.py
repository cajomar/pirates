from ink import _inkd, _ident, _ink_data_event, _ink_p, libink, Data, \
        DataListener, Ink, c_ink_ident, ctypes


libink.inkd_trigger.argtypes = [_inkd, c_ink_ident]


def trigger(self, name):
    libink.inkd_trigger(self._p, _ident(name))


Data.trigger = trigger


def get(self):
    while True:
        self._invalidate_last_event()
        e = _ink_data_event()
        if libink.inkd_listener_poll(self._p, ctypes.byref(e)):
            yield self._handle_raw_event(e)
        else:
            break


DataListener.get = get


libink.ink_getViewport.argtypes = [
        _ink_p,
        ctypes.POINTER(ctypes.c_int),
        ctypes.POINTER(ctypes.c_int)
        ]


def get_viewport(self):
    w = ctypes.c_int()
    h = ctypes.c_int()
    libink.ink_getViewport(self._p, ctypes.byref(w), ctypes.byref(h))
    return w.value, h.value


Ink.get_viewport = get_viewport
