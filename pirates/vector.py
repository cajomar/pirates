from math import sin, cos, atan2, degrees, sqrt, radians
import operator

def reg_deg(deg):
    """Returns a form of deg such that -180 <= deg <= 180"""
    deg %= 360
    if deg > 180:
        deg -= 360
    elif deg < -180:
        deg += 360
    return deg

def _vector_operator(op, is_ip):
    def f(self, c):
        _return = self if is_ip else Vec2(0, 0)
        try:
            _return.x = op(self.x, c[0])
            _return.y = op(self.y, c[1])
        except TypeError:
            if isinstance(c, (int, float)):
                _return.x = op(self.x, c)
                _return.y = op(self.y, c)
            else:
                raise TypeError(f"This arithatic is impossible for {type(self)} and {type(c)}")
        return _return
    return f

def _vector_comparison_operator(op):
    def f(self, c):
        _return = False
        try:
            _return = op(self.x, c[0]) and op(self.x, c[1])
        except TypeError:
            if isinstance(c, (int, float)):
                _return = op(self.x, c) and op(self.y, c)
            else:
                raise TypeError(f"This arithatic is impossible for {type(self)} and {type(c)}")
        return _return
    return f

class Vec2:
    """A 2D vector storing rectangular coorinates."""
    __slots__ = "x", "y"
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def set_polar(self, mag, rad):
        self.x = cos(rad) * mag
        self.y = sin(rad) * mag

    magnitude_squared = property(lambda self: self.x**2 + self.y**2)
    magnitude = property(
            lambda self: sqrt(self.magnitude_squared),
            lambda self, mag: self.set_polar(mag, self.radians))
    radians = property(
            lambda self: atan2(self.y, self.x),
            lambda self, rad: self.set_polar(self.magnitude, rad))
    degrees = property(
            lambda self: degrees(self.radians),
            lambda self, deg: self.set_polar(self.magnitude, radians(deg)))

    def normalize(self):
        return Vec2(self.x / self.magnitude, self.y / self.magnitude)
    def normalize_ip(self):
        m = self.magnitude
        self.x /= m
        self.y /= m
        return self

    def zero(self):
        """Sets self.x and self.y to 0"""
        self.x = 0
        self.y = 0

    @classmethod
    def polar(cls, magnitude, radians):
        """Returns Vec2 object with the given angle (in radians) and magnitude."""
        v2 = cls(0, magnitude)
        v2.radians = radians
        return v2

    def copy(self):
        """Returns a copy of self."""
        return self.__class__(self.x, self.y)

    def round(self, digits=0):
        """Returns a copy of self with the rectangular coordinates rounded to digits number of digits right of the decimal point.
        If digits is not given, they will be rounded to the nearest integer.
        """
        return self.__class__(round(self.x, digits), round(self.y, digits))

    def round_ip(self, digits=0):
        """Rounds self's rectangular coordinates rounded to digits number of digits right of the decimal point.
        If digits is not given, they will be rounded to the nearest integer.
        """
        self.x = round(self.x, digits)
        self.y = round(self.y, digits)

    def int(self):
        """Returns a copy of self with integer rectangular coordinates."""
        return self.__class__(int(self.x), int(self.y))

    def int_ip(self):
        """Converts self's rectangular coordinates to integer values."""
        self.x = int(self.x)
        self.y = int(self.y)

    def tupal(self):
        """Returns (self.x, self.y)"""
        return self.x, self.y

    __add__ = _vector_operator(operator.add, False)
    __sub__ = _vector_operator(operator.sub, False)
    __mul__ = _vector_operator(operator.mul, False)
    __truediv__ = _vector_operator(operator.truediv, False)
    __floordiv__ = _vector_operator(operator.floordiv, False)
    __iadd__ = _vector_operator(operator.add, True)
    __isub__ = _vector_operator(operator.sub, True)
    __imul__ = _vector_operator(operator.mul, True)
    __itruediv__ = _vector_operator(operator.truediv, True)
    __ifloordiv__ = _vector_operator(operator.floordiv, True)

    def __neg__(self):
        return Vec2(-self.x, -self.y)
    def __pos__(self):
        return Vec2(self.x, self.y)
    def __abs__(self):
        return Vec2(abs(self.x), abs(self.y))

    __lt__ = _vector_comparison_operator(operator.lt)
    __eq__ = _vector_comparison_operator(operator.eq)
    __gt__ = _vector_comparison_operator(operator.gt)
    __le__ = _vector_comparison_operator(operator.le)
    __ge__ = _vector_comparison_operator(operator.ge)
    __ne__ = _vector_comparison_operator(operator.ne)

    def __bool__(self):
        return self.x and self.y

    def __len__(self):
        return 2

    def __getitem__(self, index):
        if index == 0:
            return self.x
        elif index == 1:
            return self.y
        else:
            if isinstance(index, int):
                raise IndexError("vector index out of range")
            else:
                raise TypeError("vector indices must be type int, not", type(index))

    def __setitem__(self, index, val):
        if index == 0:
            self.x = val
        elif index == 1:
            self.y = val
        else:
            if isinstance(index, int):
                raise IndexError("vector index out of range")
            else:
                raise TypeError("vector indices must be type int, not", type(index))

    def __iter__(self):
        yield self.x
        yield self.y

    def __reversed__(self):
        yield self.y
        yield self.x

    def __repr__(self):
        return "{}({}, {})".format(self.__class__.__name__, self.x, self.y)

    def __str__(self):
        return "{}({}, {})".format(self.__class__.__name__, round(self.x, 2), round(self.y, 2))

    def __format__(self, format_spec):
        return self.__str__()
