import random

import vector
import pathfinding
import cargo
import units


class TradeCommand:
    def __init__(self, town, to_transact):
        self.target = town
        self.to_transact = to_transact


class AnchorCommand:
    def __init__(self, target):
        self.target = target


class HuntCommand:
    def __init__(self, target):
        self.target = target


commands = [TradeCommand, AnchorCommand, HuntCommand]


class ShipAI:
    AI_INTERVAL = 0.1

    def __init__(self, ship):
        self.ship = ship
        self.commands = []
        self.path = []
        self.time = 0

    def step(self, map, wind, dt, human_controlled):
        if not human_controlled:
            self.time -= dt
            if self.time < 0:
                self.tick(map, wind)
        if isinstance(self.ship, units.Fleet):
            for s in self.ship.subunits:
                s.ai.tick_in_fleet(self.ship.subunits)

    def tick(self, map, wind):
        if not self.commands:
            self.commands.append(TradeCommand(random.choice(map.towns),
                                 cargo.Cargo(*(random.randint(-1, 2) for i in
                                             range(6)))))

        self.sail_to((int(self.commands[0].target.x),
                      int(self.commands[0].target.y)),
                     map, wind)

        if self.commands and isinstance(self.commands[0], AnchorCommand) and\
                self.ship.sail_height > 0:
            self.ship.moving_sail = -1

    def tick_in_fleet(self, units):
        max_avoid_distance = 0.5
        mass = sum([s.pos for s in units if s is not self.ship])
        mass /= len(units) - 1
        mass -= self.ship.pos

        velocity = sum([s.velocity for s in units if s is not self.ship])
        velocity /= len(units) - 1
        velocity.normalize_ip()

        avoid = vector.Vec2(0, 0)
        for s in units:
            if s is self.ship:
                continue
            dist = s.pos - self.ship.pos
            if dist.magnitude_squared < max_avoid_distance ** 2:
                avoid -= dist

        v = mass + velocity + avoid
        v.radians -= self.ship.velocity.radians

        if v.y > 0:
            self.ship.turning = 1
        elif v.y < 0:
            self.ship.turning = -1
        else:
            self.ship.turning = 0

    def on_docking(self, town):
        if not self.commands:
            return
        cmd = self.commands[0]
        if town is cmd.target:
            if isinstance(cmd, TradeCommand):
                for x in range(2):
                    for i, v in enumerate(cmd.to_transact):
                        if v > 0:
                            while cargo.transact(self.ship, town, i, 100,
                                                 town.cargo_prices):
                                pass
                        if v < 0:
                            while cargo.transact(town, self.ship, i, 100,
                                                 town.cargo_prices):

                                pass

    def sail_to(self, xy, map, wind):
        sp = tuple(self.ship.pos.int())
        if (sp == xy):
            return

        if not self.path:
            self.path = pathfinding.find_path(sp, xy, map, self.ship, wind)
            if not self.path:
                self.ship.pos += (1, 1)
                self.commands.pop(0)

        while self.path and sp == self.path[-1]:
            self.path.pop()

        if not self.path:
            return

        tile = vector.Vec2(*self.path[-1])
        tile += (0.5, 0.5)

        angle = vector.reg_deg((tile - self.ship.pos).degrees
                               - self.ship.velocity.degrees)
        self.ship.turning = 1 if angle > 5 else -1 if angle < -5 else 0
