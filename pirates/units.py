import enum

from cargo import Cargo
from map import MapTileType
from baseunit import Unit
from town import Town
from ship import Ship, ShipType
from fleet import Fleet


class Character:
    def __init__(self):
        self.vehicle = None
        self.traits = [TraitType.NONE]*3


@enum.unique
class TraitType(enum.IntEnum):
    NONE = 0
    SPYGLASS = 1
    FAKE_MUSTACHE = 2


class Cannonball:
    __slots__ = "pos", "velocity", "start_pos", "from_ship"


class OverlandParty(Unit):
    enterable = MapTileType.MEADOW | MapTileType.HILLS

    def __init__(self, player, x, y):
        Unit.__init__(self, player, x, y)
        self.velocity.zero()

    max_cargo = property(lambda self: self.crew//4 + 1)


class Overboard(Unit):
    def __init__(self):
        Unit.__init__(self, 0, 0, 0)


# For the UI
def get_unit_type(unit):
    if isinstance(unit, Ship):
        return unit.type
    else:
        for i, u in enumerate((Fleet, OverlandParty, Town, Overboard,
                               Treasure)):
            if isinstance(unit, u):
                return i + len(ShipType)


class Treasure:
    def __init__(self, player, pos):
        self.player = player
        self.pos = pos
        self.gold = 0
        self.cargo = Cargo()
