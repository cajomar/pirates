import ctypes
from ink import libink

INK_MOUSE_LEFT = 0
INK_MOUSE_MIDDLE = 1
INK_MOUSE_RIGHT = 2
INK_MOUSE_X1 = 3
INK_MOUSE_X2 = 4
INK_TOUCH = 5
INK_STYLUS = 6
INK_STYLUS_B1 = 7
INK_STYLUS_B2 = 8
INK_STYLUS_B3 = 9
INK_POINTER_ANY = 10

INK_POINTER_DOWN = 0
INK_POINTER_UP = 1
INK_POINTER_MOTION = 2

INK_EVENT_TYPE_INVALID = 0
INK_EVENT_TYPE_POINTER = 1
INK_EVENT_TYPE_ACTION = 2
INK_EVENT_TYPE_TEXT = 3
INK_EVENT_TYPE_SCROLL = 4

INK_KEY_DOWN = 0
INK_KEY_REPEAT = 1
INK_KEY_UP = 2
INK_TEXT_INPUT = 3


class _ink_str(ctypes.Structure):
    _fields_ = [
            ('gencount', ctypes.c_uint32),
            ('len', ctypes.c_uint32),
            ('borrowed', ctypes.c_char_p),
            ('owned', ctypes.c_void_p),
            ]


libink.ink_str_char.argtypes = [_ink_str]
libink.ink_str_char.restype = ctypes.c_char_p


class PointerEvent(ctypes.Structure):
    _fields_ = [
            ('pointer', ctypes.c_int),
            ('action', ctypes.c_int),
            ('pointer_id', ctypes.c_int),
            ('x', ctypes.c_int),
            ('y', ctypes.c_int),
            ]


class TextInputEvent(ctypes.Structure):
    _fields_ = [
            ('_text', _ink_str),
            ('key_state', ctypes.c_int),
            ('shift', ctypes.c_int, 1),
            ('ctrl', ctypes.c_int, 1),
            ('super', ctypes.c_int, 1),
            ('alt', ctypes.c_int, 1),
            ('padding_0', ctypes.c_int, 1),
            ('padding_1', ctypes.c_int, 1),
            ('padding_2', ctypes.c_int, 1),
            ('padding_3', ctypes.c_int, 1),
            ]

    @property
    def text(self):
        t = libink.ink_str_char(self._text)
        if t:
            return t.decode()
        return ""


class ScrollEvent(ctypes.Structure):
    _fields_ = [
            ('dx', ctypes.c_int),
            ('dy', ctypes.c_int),
            ('pointer_id', ctypes.c_int),
            ]


class ActionEvent(ctypes.Structure):
    _fields_ = [
            ('action', ctypes.c_int),
            ('key_state', ctypes.c_int),
            ]


class _ink_event_data(ctypes.Union):
    _fields_ = [
            ('pointer', PointerEvent),
            ('action', ActionEvent),
            ('text', TextInputEvent),
            ('scroll', ScrollEvent),
            ]


class Event(ctypes.Structure):
    _fields_ = [
            ('event_type', ctypes.c_int),
            ('data', _ink_event_data),
            ]

    def __getattr__(self, field):
        print("Getting", field)
        if self.event_type == INK_EVENT_TYPE_POINTER:
            return getattr(self.data.pointer, field)
        elif self.event_type == INK_EVENT_TYPE_ACTION:
            return getattr(self.data.action, field)
        elif self.event_type == INK_EVENT_TYPE_TEXT:
            return getattr(self.data.text, field)
        elif self.event_type == INK_EVENT_TYPE_SCROLL:
            return getattr(self.data.scroll, field)
