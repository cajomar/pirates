import sys
import os

import lib2d
import ink
import ink.runner
import extink  # noqa
import extink_runner  # noqa

import render
import scene

root_scene = None


def post_init():
    lib2d.init()

    global main_listener
    main_listener = ink.DataListener()
    main_data = ink.source("main")
    main_data.add_listener(main_listener)
    render.init()

    if "-p" in sys.argv:
        main_data.set_bool("in_game", True)
        start_game()


def pre_step():
    if root_scene:
        w, h = ctx.get_viewport()
        render.screen_width = w
        render.screen_height = h
        lib2d.viewport(w, h)
        render.draw(root_scene.get_renderable_scene())
        lib2d.render()
        ink.source("game").set_int("scene", root_scene.get_active_scene().id)


def post_step(dt):
    for ev in main_listener.get():
        if ev.type == 'CHANGE':
            if ev.field == 'in_game':
                if ev.data.get_bool(ev.field):
                    start_game()
                else:
                    global root_scene
                    root_scene = None
        elif ev.type == "IMPULSE":
            if ev.field == "quit":
                ink.runner.exit()

    if root_scene:
        root_scene.step(dt)
        render.draw_world(root_scene)
        lib2d.render()


def main():
    global ctx
    ctx = ink.Ink()
    ink.runner.run(gui_bin_path="gui.bin",
                   assets_bin_path="gui_assets.bin",
                   net_path=os.path.join("gui", "runner.ipc"),
                   window_title="The Sea-Dog's Grandson",
                   window_width=1024,
                   window_height=800,
                   post_init=post_init,
                   post_step=post_step,
                   pre_step=pre_step,
                   continuous_step=True)

    lib2d.shutdown()


def start_game():
    ink.source("game").clear_all()
    global root_scene
    root_scene = scene.Main()


if __name__ == "__main__":
    main()
