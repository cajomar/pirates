import enum
import random

from map import MapTileType
from baseunit import Unit
import vector
import ai
import cargo
import pathfinding


@enum.unique
class ShipType(enum.IntEnum):
    SKIFF = 0
    SLOOP = 1
    MERCHANTMAN = 2
    GALLEON = 3


SHIP_SETTINGS = (
        # name mincrew maxcrew maxcargo maxcannon lenth width turnspeed
        ("skiff",           1,  12,     10,     1,  0.1, 0.05, 120),
        ("sloop",           5,  70,     100,    10, 0.3, 0.15, 100),
        ("merchantman",     10, 130,    200,    20, 0.4, 0.2, 80),
        ("galleon",         20, 250,    400,    40, 0.5, 0.3, 50),
)


def ship_settings_wrapper(index):
    def f(self):
        return SHIP_SETTINGS[self.type][index]
    return f


class Ship(Unit):
    AI_TIME = 0.1
    enterable = MapTileType.WATER
    min_crew = property(ship_settings_wrapper(1))
    max_crew = property(ship_settings_wrapper(2))
    max_cargo = property(ship_settings_wrapper(3))
    max_combat_cannon = property(ship_settings_wrapper(4))
    # Width and height of a ship. Since rotation points east and goes clockwise
    # (because +y points south), `length` corosponds with the x axis and `beam`
    # with the y axis. Kinda weird.
    length = property(ship_settings_wrapper(5))
    beam = property(ship_settings_wrapper(6))
    turn_speed = property(ship_settings_wrapper(7))

    def __init__(self, player, ship_type, x, y):
        Unit.__init__(self, player, x, y)
        self.velocity = vector.Vec2.polar(0.5, random.randrange(360))
        self.turning = 0
        self.type = ship_type
        # The happiness of the crew, from 0 (mutinus) to 1 (exubrant). This is
        # based on how much food they eat, tobacco they smoke, coffee they
        # dink, distilled sugar they drink, and (maybe) how bright their
        # clothes are (unless indigo is just a trade item) and how many ships
        # they capture.
        self.crew_morale = 0.7
        self.sail_height = 1
        self.moving_sail = 0

        self.commands = []
        self.path = []
        self.time = 0

    def move(self, wind, get_tile_type, dt):
        self.sail_height += dt * self.moving_sail
        self.velocity.degrees += self.turn_speed * dt * self.turning
        self.calculate_speed(wind)
        super().move(get_tile_type, dt)

    flagship = property(lambda self: self)

    def calculate_speed(self, wind):
        apparent_wind = vector.Vec2.polar(wind.magnitude,
                                          self.velocity.radians)
        # air_density = 1.0
        # sail_size = 0.5

        # lift = apparent_wind.radians + math.pi/2
        # drag = lift*lift / (0.5 * 1.0 * 1**2 * math.pi)
        # total_force = lift + drag

        self.velocity = apparent_wind * self.sail_height
        return

        if self.type == ShipType.SKIFF:
            return max(0.1, self.crew / self.max_crew * 1)
        elif self.type == ShipType.SLOOP:
            local_wind_deg = (wind - self.velocity).degrees
            local_wind_deg %= 90
            local_wind_deg -= 45
            return abs(local_wind_deg)/45
        elif self.type == ShipType.MERCHANTMAN:
            local_wind_deg = vector.reg_deg(wind.degrees
                                            - self.velocity.degrees)
            return (180 - abs(local_wind_deg)) / 180 * 1
        elif self.type == ShipType.GALLEON:
            local_wind_deg = vector.reg_deg(wind.degrees
                                            - self.velocity.degrees)
            return (180 - abs(local_wind_deg)) / 180 * 1
        # elif self.type == ShipType.OVERLAND_PARTY:
            # return max(0.2, tools.proportion_exp(50, 1, self.crew, -0.3))
        else:
            return 1

    def ai_step(self, map, wind, dt, human_controlled):
        if not human_controlled:
            self.time -= dt
            if self.time < 0:
                self.time = self.AI_TIME
                self.ai_tick(map, wind)

    def ai_tick(self, map, wind):
        if not self.commands:
            self.commands.append(ai.TradeCommand(random.choice(map.towns),
                                 cargo.Cargo(*(random.randint(-1, 2) for i in
                                             range(6)))))

        self.sail_to((int(self.commands[0].target.x),
                      int(self.commands[0].target.y)),
                     map, wind)

        if self.commands and isinstance(self.commands[0], ai.AnchorCommand) \
                and self.sail_height > 0:
            self.moving_sail = -1

    def ai_on_docking(self, town):
        if not self.commands:
            return
        cmd = self.commands[0]
        if town is cmd.target:
            if isinstance(cmd, ai.TradeCommand):
                for x in range(2):
                    for i, v in enumerate(cmd.to_transact):
                        if v > 0:
                            while cargo.transact(self, town, i, 100,
                                                 town.cargo_prices):
                                pass
                        if v < 0:
                            while cargo.transact(town, self, i, 100,
                                                 town.cargo_prices):

                                pass

    def sail_to(self, xy, map, wind):
        sp = tuple(self.pos.int())
        if (sp == xy):
            return

        if not self.path:
            self.path = pathfinding.find_path(sp, xy, map, self, wind)
            if not self.path:
                self.pos += (1, 1)
                self.commands.pop(0)

        while self.path and sp == self.path[-1]:
            self.path.pop()

        if not self.path:
            return

        tile = vector.Vec2(*self.path[-1])
        tile += (0.5, 0.5)

        angle = vector.reg_deg((tile - self.pos).degrees
                               - self.velocity.degrees)
        self.turning = 1 if angle > 5 else -1 if angle < -5 else 0
